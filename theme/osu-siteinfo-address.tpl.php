<?php
/**
 * @file
 * Display the address portion of an hcard in a block.
 *
 * Available variables:
 *
 * - $street_address: The street-address value.
 * - $extended_address: The extended-address value.
 * - $locality: The locality value. In USA, this is the city.
 * - $region: The region value. In the USA, this is the state.
 * - $postal_code: The postal-code value.
 * - $country: The country-name value.
 */
?>
<?php if ($street_address || $locality || $region || $postal_code || $country): ?>
  <div class="adr">
    <?php if ($extended_address): ?>
      <div class="extended-address"><?php print $extended_address; ?></div>
    <?php endif; ?>
    <?php if ($street_address): ?>
      <div class="street-address"><?php print $street_address; ?></div>
    <?php endif; ?>
    <?php if ($locality): ?>
      <span class="locality"><?php print $locality; ?></span>,
    <?php endif; ?>
    <?php if ($region): ?>
      <span class="region"><?php print $region; ?></span>
    <?php endif; ?>
    <?php if ($postal_code): ?>
      <span class="postal-code"><?php print $postal_code; ?></span>
    <?php endif; ?>
  </div>
<?php endif; ?>

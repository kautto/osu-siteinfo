<?php
/**
 * @file
 * Default theme implementation to display the hcard in a block.
 *
 * Available variables:
 *
 * - $site_name_prefix: A prefix such as "Department of"
 * - $site_name: The name of the business or organization.
 *
 * - $address: The full address in vcard format.
 *
 * - $phone: single phone number
 * - $fax: single fax number
 *
 * - $email_url: The href for the mailto link.
 * - $email: The email address value.
 */
?>
<div class="vcard">

  <?php if ($site_name): ?>
    <div class="fn org">
      <?php if ($site_name_prefix):?>
        <span class="site-name-prefix"><?php print $site_name_prefix; ?></span>
      <?php endif; ?>
      <span class="site-name"><?php print $site_name; ?></span>
    </div>
  <?php endif; ?>

  <?php if ($address): ?><?php print $address; ?><?php endif; ?>

  <?php if ($phone || $fax): ?>
    <div class="phone">
      <?php if ($phone): ?>
        <div class="tel"><abbr class="type" title="voice"><?php print t('Telephone'); ?>:</abbr> <?php print $phone; ?></div>
      <?php endif; ?>
      <?php if ($fax): ?>
        <div class="tel"><abbr class="type" title="fax"><?php print t('Fax'); ?>:</abbr> <?php print $fax; ?></div>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <?php if ($email): ?>
    <a href="<?php print $email_url; ?>" class="email"><?php print $email; ?></a>
  <?php endif; ?>

</div>

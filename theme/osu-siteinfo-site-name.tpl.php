<?php
/**
 * @file
 * Default theme implementation to display the site name.
 *
 * Available variables:
 *
 * - $site_name_prefix: A prefix such as "Department of"
 * - $site_name: The name of the business or organization.
 */
?>
<?php if ($site_name_prefix):?>
  <span class="site-name-prefix"><?php print $site_name_prefix; ?></span>
<?php endif; ?>
<span class="site-name"><?php print $site_name; ?></span>

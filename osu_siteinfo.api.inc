<?php

/**
 * @file
 *
 * Contains hook definitions.
 */

/**
 * Example implementation of hook_osu_siteinfo().
 *
 * Each section will become a vertical tab in the main site information form.
 */
function hook_osu_siteinfo_section() {
  return array(
    'my_section' => array(
      'title' => t('My Section'),
      'weight' => 50,
      'form' => array(),
      'validate' => array(),
      'submit' => array(),
    ),
  );
}

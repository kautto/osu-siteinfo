<?php

/**
 * @file
 *
 * Administrative forms for osu_siteinfo.
 */

/**
 * Assembles the site information form.
 */
function osu_siteinfo_form() {

  $form = array();
  $form['tabs'] = array(
    '#type' => 'vertical_tabs',
  );

  $sections = osu_siteinfo_sections();
  foreach ($sections as $name => $vals) {
    $form['tabs'][$name] = array(
      '#type' => 'fieldset',
      '#title' => $vals['title'],
      '#tree' => TRUE,
    );
    $form['tabs'][$name] = array_merge($form['tabs'][$name], $vals['form']);
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#title' => t('Save'),
    '#value' => 'Save',
  );
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function osu_siteinfo_form_validate($form, &$form_state) {
  $sections = osu_siteinfo_sections();
  foreach ($sections as $name => $vals) {
    if (isset($vals['validate']) && is_array($vals['validate'])) {
      foreach ($vals['validate'] as $callback) {
        $callback($form_state[$name]);
      }
    }
  }
}

/**
 * Implements hook_form_submit().
 */
function osu_siteinfo_form_submit($form, &$form_state) {
  $sections = osu_siteinfo_sections();
  foreach ($sections as $name => $vals) {
    if (isset($vals['submit']) && is_array($vals['submit'])) {
      foreach ($vals['submit'] as $callback) {
        $callback($form_state['values'][$name]);
      }
    }
  }
}

/**
 * Simplified replacement of system_settings_form_submit (allows isolation).
 *
 * @param array $values
 *   Associative array of variable names and values.
 */
function osu_siteinfo_system_settings_form_submit(array &$values) {
  foreach ($values as $key => $value) {
    variable_set($key, $value);
  }
}

/**
 * Returns a weight sorted list array of hook_osu_siteinfo_section returns.
 */
function osu_siteinfo_sections() {
  $sections = &drupal_static(__FUNCTION__);
  if (!isset($sections)) {
    $sections = module_invoke_all('osu_siteinfo_section');
    uasort($sections, 'drupal_sort_weight');
  }
  return $sections;
}

# OSU Site Information

This module provides an extensible, consolidated site information
configuration form. The goal is to allow a non-technical person
to enter the content-esque configuration, easily, in one place.

While it was intended to work with Open OSU, it is loosely coupled
and should work for a variety of purposes.

## Accessing the form

The form is exposed two different ways.

* under site configuration > site information
* under apps > site information

## Extending the form

There are two ways to extend the form.

Modules can define their own vertical tab sections
by implementing hook_osu_siteinfo_section() which is
documented in the api.inc. For example, one included
section contains contact information (address, phone, email).
Another module might implement a social networking section.

In addition, you can always use hook_form_alter().

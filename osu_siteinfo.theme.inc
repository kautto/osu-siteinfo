<?php

/**
 * @file
 *
 * Theme helper functions.
 */

/**
 * Implements hook_preprocess_osu_siteinfo_address().
 */
function template_preprocess_osu_siteinfo_address(&$variables) {

  $map = array(
    'osu_siteinfo_street_address' => 'street_address',
    'osu_siteinfo_extended_address' => 'extended_address',
    'osu_siteinfo_locality' => 'locality',
    'osu_siteinfo_region' => 'region',
    'osu_siteinfo_postal_code' => 'postal_code',
    'osu_siteinfo_country' => 'country',
  );
  $variables = array_merge($variables, osu_siteinfo_variables_load($map));

}

/**
 * Implements hook_preprocess_osu_siteinfo_hcard().
 */
function template_preprocess_osu_siteinfo_hcard(&$variables) {
  $map = array(
    'site_name' => 'site_name',
    'osu_siteinfo_site_name_prefix' => 'site_name_prefix',
    'osu_siteinfo_email' => 'email',
    'osu_siteinfo_phone' => 'phone',
    'osu_siteinfo_fax' => 'fax',

  );
  $variables = array_merge($variables, osu_siteinfo_variables_load($map));

  if (function_exists('invisimail_encode_html')) {
    $variables['email'] = invisimail_encode_html($variables['email']);
    $variables['email_url'] = INVISIMAIL_MAILTO_ASCII . $variables['email'];
  }
  else {
    $variables['email'] = $variables['email'];
    $variables['email_url'] = 'mailto:' . $variables['email'];
  }

  $variables = array_merge($variables, osu_siteinfo_variables_load($map));
  $variables['address'] = theme('osu_siteinfo_address');
}

/**
 * Implements hook_preprocess_osu_siteinfo_site_name().
 */
function template_preprocess_osu_siteinfo_site_name(&$variables) {

  $map = array(
    'site_name' => 'site_name',
    'osu_siteinfo_site_name_prefix' => 'site_name_prefix',
  );
  $variables = array_merge($variables, osu_siteinfo_variables_load($map));
}

/**
 * Translates system variables into escaped/encoded theme variables.
 *
 * @param array $variable_map
 *   Associative array mapping system variable names to template variable names.
 * @param string $callback
 *   Callback to sanitize the result.
 *
 * @return array
 *   Associative array of sanitized variables.
 */
function osu_siteinfo_variables_load(array $variable_map, $callback = 'check_plain') {
  $variables = array();
  foreach ($variable_map as $variable => $name) {
    $variables[$name] = $callback(variable_get_value($variable));
  }
  return $variables;
}

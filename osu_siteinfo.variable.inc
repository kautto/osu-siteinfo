<?php

/**
 * Implements hook_variable_info().
 */
function osu_siteinfo_variable_info($options) {

  $variables = array();
  $variables['osu_siteinfo_site_name_prefix'] = array(
    'type' => 'string',
    'title' => t('Prefix'),
    'description' => t('Examples: Department of, Center for, John Smith Laboratory for, etc'),
    'default' => t('Department/Center/Lab for'),
  );
  $variables['osu_siteinfo_site_type'] = array(
    'type' => 'string',
    'title' => t('What type of site is this?'),
    'description' => t('Select your official designation or the closest match.'),
    'default' => 'college',
    'options' => array(
      'official' => t('Major Academic & Research Organizations'),
      'college' => ' - ' . t('College'),
      'department' => ' - ' . t('Department'),
      'institute' => ' - ' . t('Institute'),
      'center' => ' - ' . t('Center'),
      'support' => t('Supporting Groups'),
      'student_services' => ' - ' . t('Student Services'),
      'business_services' => ' - ' . t('Business Services'),
      'other' => t('Other Groups'),
      'lab' => ' - ' . t('Faculty Research Lab'),
      'student_org' => ' - ' . t('Student Organization'),
      'other' => t('Other'),
    ),
  );
  $variables['osu_siteinfo_affiliation'] = array(
    'type' => 'string',
    'title' => t('With which unit is the website primarily affiliated?'),
    'description' => t('Options here correspond to approved secondary signatures.'),
    'default' => 'engineering',
    'options callback' => osu_siteinfo_affiliation_options(),
  );
   $variables['osu_siteinfo_deptid'] = array(
    'type' => 'string',
    'title' => t('Department Id'),
    'description' => t('Department ids are 5 digit numbers assigned to official OSU organizations.'),
    'default' => '',
  );

  return $variables;
}

